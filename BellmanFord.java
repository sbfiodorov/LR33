import java.util.Arrays;

public class BellmanFord
{
    String[] data;
    int[][] edges;
    MyList<String> vertices = new MyList<>();

    public BellmanFord(String[] data)
    {
        this.data = data;
    }

    public int[][] run()
    {
        getVertices();
        getEdges();

        int[][] ints = new int[vertices.size()][vertices.size()];
        for (int i = 0; i < ints.length; i++)
        {
            for (int j = 0; j < ints[i].length; j++)
            {
                if (i == j)
                {
                    ints[i][j] = 0;
                }
                else
                {
                    ints[i][j] = 10000;
                }
            }
        }

        for (int i = 0; i < ints.length; i++)
        {
            for (int j = 0; j < vertices.size() - 1; j++)
            {
                for (int[] edge : edges)
                {
                    int from = ints[i][edge[0]];
                    int to = ints[i][edge[1]];
                    int w = edge[2];
                    if (from + w < to)
                    {
                        ints[i][edge[1]] = from + w;
                    }
                }
            }
        }
        return ints;
    }

    private void getVertices()
    {
        for (int i = 0; i < data.length; i++)
        {
            if (i % 4 == 0 || i % 4 == 1)
            {
                if (!vertices.contains(data[i]))
                {
                    vertices.add(data[i]);
                }
            }
        }

    }

    private void getEdges()
    {
        edges = new int[data.length / 2][3];
        int edgesNum = 0;

        for (int i = 0; i < data.length; i++)
        {
            if (i % 4 == 2)
            {
                try
                {
                    edges[edgesNum][0] = vertices.indexOf(data[i - 2]);
                    edges[edgesNum][1] = vertices.indexOf(data[i - 1]);
                    edges[edgesNum][2] = Integer.parseInt(data[i]);
                    edgesNum++;
                }
                catch
                (NumberFormatException e)
                {
                    System.out.println();
                }

            }
            else if (i % 4 == 3)
            {
                try
                {
                    edges[edgesNum][0] = vertices.indexOf(data[i - 2]);
                    edges[edgesNum][1] = vertices.indexOf(data[i - 3]);
                    edges[edgesNum][2] = Integer.parseInt(data[i]);
                    edgesNum++;
                }
                catch (NumberFormatException e)
                {
                    System.out.println();
                }
            }
        }
        edges = Arrays.copyOfRange(edges, 0, edgesNum);
    }

    public void printMatrix()
    {
        int[][] result = new BellmanFord(data).run();

        System.out.println("Матрица смежности : ");
        for (int[] ints : result)
        {
            for (int anInt : ints)
            {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}