public class Main
{
    private static final String file = "data/input.txt";

    public static void main(String[] args)
    {
        MyReader reader = new MyReader();
        String[] data = reader.read(file).split(";");
        BellmanFord blf = new BellmanFord(data);
        blf.printMatrix();
    }
}