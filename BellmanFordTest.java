import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class BellmanFordTest
{
    String[] data1 = {"Санкт-Петербург", "Москва", "10", "20"};
    String[] data2 = {"Владивосток", "Хабаровск","13","8"};

    int[][] result1 =
    {
            {0, 10},
            {20, 0}
    };

    int[][] result2 =
    {
            {0, 13},
            {8, 0}
    };

    @Test
    public void BellmanFloydRunTest()
    {
        Assertions.assertArrayEquals(result1, new BellmanFord(data1).run());
        Assertions.assertArrayEquals(result2, new BellmanFord(data2).run());
    }
}